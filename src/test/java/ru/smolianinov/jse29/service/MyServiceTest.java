package ru.smolianinov.jse29.service;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import static org.junit.jupiter.api.Assertions.*;

class MyServiceTest {
    MyService myService;

    @RepeatedTest(3)
    void sum() {
        MyService myService = new MyService();
        assertEquals(12, myService.sum("7", "5"));
    }

    @Test
    void sumException() {
        //final MyService myService = new MyService();
        assertThrows(IllegalArgumentException.class, () -> myService.sum("1a", "3"));
    }


    @Test
    void parseNumFibonachi() {
        long[] expect = {0, 1, 2, 3, 4, 5};
        final MyService myService = new MyService();
        long[] result = myService.parseNumFibonachi("5");
        assertTrue(new ReflectionEquals(expect).matches(result));
    }

    @Test
    void parseNumFibonachiException() {
        final MyService myService = new MyService();
        assertThrows(IllegalArgumentException.class, () -> myService.parseNumFibonachi("f"));
    }

    @Test
    void calcFactorial() {
        MyService myService = new MyService();
        assertEquals(120, myService.calcFactorial("5"));
    }

    @Test
    void calcFactorialException() {
        final MyService myService = new MyService();
        assertThrows(IllegalArgumentException.class, () -> myService.calcFactorial("f"));
    }

}