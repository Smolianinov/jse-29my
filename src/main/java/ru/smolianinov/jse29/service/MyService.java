package ru.smolianinov.jse29.service;

import java.util.*;

public class MyService {

    public long sum(String arg1, String arg2) {
        long num1;
        long num2;
        num1 = Integer.parseInt(arg1.trim());
        num2 = Integer.parseInt(arg2.trim());
        return (num1 + num2);
    }

    public long calcFactorial(String arg) throws IllegalArgumentException {
        long num;
        try {
            num = Integer.parseInt(arg.trim());
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException();
        }
        if (num <= 1) {
            return 1L;
        } else {
            return num * calcFactorial(Long.toString(num - 1));
        }
    }

    public long[] parseNumFibonachi(String arg) throws IllegalArgumentException {
        long num;
        HashMap<Long, Long> listNum = new LinkedHashMap<>();
        long cnt = 1;
        try {
            num = Integer.parseInt(arg.trim());
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException();
        }
        listNum.put(0L, 1L);
        listNum.put(1L, 1L);
        while (listNum.get(cnt) != num) {
            cnt++;
            listNum.put(cnt, listNum.get(cnt - 2) + listNum.get(cnt - 1));

        }
        //Для возврата типа результата метода согласно задания,
        //создаю простой массив и записываю в него LinkedHashMap
        long[] res = new long[listNum.size()];
        int i = 0;
        Set set = listNum.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry item = (Map.Entry) iterator.next();
            res[i] = (long) item.getValue();
            i++;
        }
        return res;

    }

    private String toString(Long sum) {
        return null;
    }

}
