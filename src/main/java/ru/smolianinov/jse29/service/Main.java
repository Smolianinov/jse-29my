package ru.smolianinov.jse29.service;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        MyService myService = new MyService();
        Scanner scanner = new Scanner(System.in);

        String command = "help";
        while (!Objects.equals(command, "exit")) {
            switch (command) {
                case "sum": {
                    System.out.print("Введите аргумент 1: ");
                    String inp1 = scanner.nextLine();
                    System.out.print("Введите аргумент 2: ");
                    String inp2 = scanner.nextLine();
                    System.out.print("Сумма аргументов: ");
                    System.out.println(myService.sum(inp1, inp2));
                    break;
                }
                case "factorial": {
                    System.out.print("Введите аргумент: ");
                    String inp = scanner.nextLine();
                    System.out.print("Результат: ");
                    System.out.println(myService.calcFactorial(inp));
                    break;
                }
                case "fibonachi": {
                    System.out.print("Введите аргумент: ");
                    String inp = scanner.nextLine();
                    System.out.print("Результат: ");
                    System.out.println(Arrays.toString(myService.parseNumFibonachi(inp)));
                    break;
                }
                case "help": {
                    System.out.println("Список команд: ");
                    System.out.println("    sum - Вычисление суммы");
                    System.out.println("    factorial - Вычисленить факториал");
                    System.out.println("    fibonachi - Разложмть на сумму чисел Фибоначчи");
                    System.out.println("    help - Помощь");
                    System.out.println("    exit - Выход");
                    break;
                }
            }
            System.out.println("");
            System.out.print("Введите команду: ");
            command = scanner.nextLine();
        }
    }
}
